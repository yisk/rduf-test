
![alt text](logo/facebook_cover_photo_1.png "logo")


**bold**

_italic_

> uuuu

`test code`

[test com](http://test.com)

- 122
- 222

1. 11
1. 222
1. 233

- [x] 22222


| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |
| cell | cell |
| cell | cell |
| cell | cell |









# test site

* Web: https://issaku.gitlab.io/rduf-data-pages-hugo/page/index/

* Markdown: https://gitlab.com/issaku/rduf-data-pages-hugo/-/blob/master/content/page/_index.md

# GitLab Flavored Markdown

* https://docs.gitlab.com/ee/user/markdown.html


## ○ Headers

# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
======

Alt-H2
------


## ○ Images

Inline-style (hover to see title text):

![alt text](https://docs.gitlab.com/ee/user/img/markdown_logo.png "Title Text")

Reference-style (hover to see title text):

![alt text1][logo]

[logo]: https://docs.gitlab.com/ee/user/img/markdown_logo.png "Title Text"


## ○ Inline HTML


<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well. HTML <em>tags</em> do <b>work</b>, in most cases.</dd>
</dl>


## ○ Line breaks

This longer line is separated from the 


one above by two newlines, so it is a *separate paragraph*.



## ○ Newlines

This longer line is separated from the  
one above by two newlines, so it is a *separate paragraph*.

## ○ Links

- This is an [inline-style link](https://www.google.com)

## ○ Lists

1. First ordered list item
2. Another item
   - Unordered sub-list.
3. Actual numbers don't matter, just that it's a number
   1. Ordered sub-list
   2. Next ordered sub-list item
4. And another item.

## ○ Superscripts / Subscript

The formula for water is H<sub>2</sub>O
while the equation for the theory of relativity is E = mc<sup>2</sup>.

## ○ Tables

| header 1 | header 2 | header 3 | header 4 |
| ---      | ---      | ---      | ---      |
| cell 1   | cell 2   | cell 3   |    i     |
| cell 4 | cell 5 is longer | cell 6 . |  sss |
| cell 7   |          | cell 9   |   asbkahbk |


| Left Aligned | Centered | Right Aligned |
| :---         | :---:    | ---:          |
| Cell 1       | Cell 2   | Cell 3        |
| Cell 4       | Cell 5   | Cell 6        |


## ○ Task lists

- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

1. [x] Completed task
1. [ ] Incomplete task
   1. [ ] Sub-task 1
   1. [x] Sub-task 2

## ○ Inline diff

- {+ addition 1 +}
- [+ addition 2 +]
- {- deletion 3 -}
- [- deletion 4 -]





## ○ Colors

- `#F00`
- `#F00A`
- `#FF0000`
- `#FF0000AA`
- `RGB(0,255,0)`
- `RGB(0%,100%,0%)`
- `RGBA(0,255,0,0.3)`
- `HSL(540,70%,50%)`
- `HSLA(540,70%,50%,0.3)`

## ○ Diagrams and flowcharts

### ○○ Mermaid
```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```


